Device Drivers > USB support
  <*>   USB Gadget Support  --->
  
   > Device Drivers > USB support > USB Gadget Support ────────────────────
  ┌─────────────────────── USB Gadget Support ────────────────────────┐
  │  Arrow keys navigate the menu.  <Enter> selects submenus ---> (or │
  │  empty submenus ----).  Highlighted letters are hotkeys.          │
  │  Pressing <Y> includes, <N> excludes, <M> modularizes features.   │
  │  Press <Esc><Esc> to exit, <?> for Help, </> for Search.  Legend: │
  │ ┌───────────────────────────────────────────────────────────────┐ │
  │ │    --- USB Gadget Support                                     │ │
  │ │    [ ]   Debugging information files (DEVELOPMENT)            │ │
  │ │    (500) Maximum VBUS Power usage (2-500 mA)                  │ │
  │ │    (2)   Number of storage pipeline buffers                   │ │
  │ │    [ ]   Serial gadget console support                        │ │
  │ │          USB Peripheral Controller  --->                      │ │
  │ │    < >   USB Gadget functions configurable through configfs   │ │
  │ │    <M>   USB Gadget precomposed configurations                │ │
  │ │    < >     Gadget Zero (DEVELOPMENT)                          │ │
  │ │    <M>     Ethernet Gadget (with CDC Ethernet support)        │ │
  │ │    [*]       RNDIS support                                    │ │
  │ │    [*]       Ethernet Emulation Model (EEM) support           │ │
  │ │    < >     Network Control Model (NCM) support                │ │
  │ │    <M>     Gadget Filesystem                                  │ │
  │ │    <M>     Function Filesystem                                │ │
  │ │    [*]       Include configuration with CDC ECM (Ethernet)    │ │
  │ │    [*]       Include configuration with RNDIS (Ethernet)      │ │
  │ │    [*]       Include 'pure' configuration                     │ │
  │ │    <M>     Mass Storage Gadget                                │ │
  │ │    <M>     Serial Gadget (with CDC ACM and CDC OBEX support)  │ │
  │ │    < >     Printer Gadget                                     │ │
  │ │    <M>     CDC Composite Device (Ethernet and ACM)            │ │
  │ │    < >     CDC Composite Device (ACM and mass storage)        │ │
  │ │    < >     Multifunction Composite Gadget                     │ │
  │ │    < >     HID Gadget                                         │ │
  │ │    < >     EHCI Debug Device Gadget                           │ │
  │ │                                                               │ │
  │ └───────────────────────────────────────────────────────────────┘ │
  ├───────────────────────────────────────────────────────────────────┤
  │     <Select>    < Exit >    < Help >    < Save >    < Load >      │
  └───────────────────────────────────────────────────────────────────┘
  
modprobe g_serial

# /etc/inittab
# Put a getty on the serial port
/dev/ttyGS0::respawn:/sbin/getty -L  /dev/ttyGS0 115200 vt100 # GENERIC_SERIAL

modprobe g_ether

ifconfig usb0 192.169.0.2 up

modprobe g_mass_storage file=/mass_storage

# /etc/init.d/S40network
case "$1" in
  start)
        printf "Starting g_serial: "
        /sbin/modprobe g_serial
#       printf "Starting g_ether: "
#       /sbin/modprobe g_ether
#       /sbin/ifconfig usb0 192.168.0.2 up

PI USB to Serial (COM79)









