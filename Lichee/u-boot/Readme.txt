#!bin/bash

PATH=$PATH:`pwd`/gcc-linaro-7.2.1-2017.11-x86_64_arm-linux-gnueabi/bin
export ARCH=arm
export CROSS_COMPILE=arm-linux-gnueabi-

cd u-boot
make menuconfig
# Disable delay
(0) delay in seconds before automatically booting

make

dd if=u-boot-sunxi-with-spl.bin of=/dev/sdX bs=1024 seek=8

//==============
# use gcc-linaro-7.2.1-2017.11-x86_64_arm-linux-gnueabi.tar.xz
# tar xvf /media/sf_projects/LicheeNano/SDK/02_u-boot/f1c100s_uboot-steward-fu.tar.gz
# /media/sf_projects/LicheeNano/SDK/tools/gcc-linaro-7.2.1-2017.11-x86_64_arm-linux-gnueabi.tar.xz

cd /media/nhi/EXT4-1T/nano/
tar xvf /media/sf_projects/LicheeNano/SDK/02_u-boot/f1c100s_uboot-steward-fu.tar.gz
cd f1c100s_uboot-steward-fu

make ARCH=arm licheepi_nano_spiflash_defconfig
make ARCH=arm -j8

# mmc
Device     Boot  Start     End Sectors  Size Id Type
/dev/sdh1         2068  206847  204780  100M  c W95 FAT32 (LBA)
/dev/sdh2       206848 1968127 1761280  860M 83 Linux

# cp bootloader
$ dd if=u-boot-sunxi-with-spl.bin of=/dev/sdh bs=1K seek=8